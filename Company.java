package org.xploration.team2.company;

import org.xploration.ontology.XplorationOntology;

import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.core.*;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.*;
import jade.content.lang.sl.*;
import jade.content.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;

import org.xploration.ontology.*;
import org.xploration.team2.Constants;

public class Company extends Agent {
    private final int teamId = 2;
    
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    
    protected void setup() {
        System.out.println(getLocalName()+": has entered into the system");
        //Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        
        addBehaviour(registerCompany());
    }
    
    private Behaviour registerCompany() {
        return new SimpleBehaviour(this) {
            
            private boolean requestedRegistration = false;
            
            @Override
            public void action() {
                
                // Creates the description for the type of agent to be searched
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(Constants.REGISTRATION_DESK_SERVICE_NAME);
                dfd.addServices(sd);
                
                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);
                    
                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        AID registrationDeskAgent = res[0].getName();
                        
                        // Asks the estimation to the painter
                        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                        msg.addReceiver(registrationDeskAgent);
                        msg.setLanguage(codec.getName());
                        msg.setOntology(ontology.getName());
                        RegistrationRequest regRequest = new RegistrationRequest();
                        Team ownTeam = new Team();
                        ownTeam.setTeamId(teamId);
                        regRequest.setTeam(ownTeam);
                        // As it is an action and the encoding language the SL, it must be wrapped
                        // into an Action
                        Action agAction = new Action(registrationDeskAgent, regRequest);
                        try {
                            // The ContentManager transforms the java objects into strings
                            getContentManager().fillContent(msg, agAction);
                            send(msg);
                            System.out.println(getLocalName()+": REQUESTS A REGISTRATION");
                            
                            requestedRegistration = true;
                        }
                        catch (CodecException | OntologyException e) {
                            e.printStackTrace();
                            doWait(10000);
                        }
                    } else {
                        // If no RegistrationDesk has been found, wait 5 seconds
                        doWait(5000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean done() {
                return requestedRegistration;
            }
        };
    }
}
